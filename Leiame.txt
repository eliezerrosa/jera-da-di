=======================
Jera DA/DI (18/04/2016)
=======================

Devido as dificuldades encontradas em efetuar leituras e geração de arquivos magnéticos referente a Depósitos Identificados e Débitos Autorizados (antigo Débito automático), iniciamos o desenvolvimento de rotinas para nos auxiliar.

Essas unit's não são componentes, portanto para utilizá-las, adicione o path onde as mesmas estão localizadas na Library do Delphi.

As unit's fazem uso de algumas rotinas que estão disponíveis nos componentes ACBr: (svn://svn.code.sf.net/p/acbr/code/trunk2).

Essas unit’s foram desenvolvidas para serem utilizadas em Delphi® a partir da versão 2009, pois utilizamos recursos que foram implementados a partir dessa versão, tais como Generics, Rtti e outras.

Todas as classes são “interfaceadas”, o que facilita e muito a utilização das mesmas.

Abaixo, tem uma breve explicação de como utilizar essas unit’s.

Os manuais dos mesmos encontram-se nas pastas, separadas por bancos. Recomendamos a leitura desses, pois senão ficará impossível de implementação.

======================
Depósito Identificado:
======================
  Não possui geração de arquivo remessa, pois o mesmo pode ser utilizado em mais de um banco, informando um ID para o caixa.

  Os bancos solicitam um identificador com dígito para aceitarem o depósito. Escolhemos o cálculo de dígitos utilizando o módulo 10.

Exemplo:
uses Jera.DI;

procedure LerRetornoDI(ABanco: TJeraDIBancos; AFileName: string);
var
  ORetorno: IJeraDI;
begin
  // Cria a classe conforme banco informado no parâmetro
  ORetorno := TJeraDI.Create(ABanco);
  // Abre o arquivo selecionado acima e processa seu conteúdo
  ORetorno.Processar(AFileName);
  
  // Acessar as property's do Header do Arquivo
  ORetorno.Header.NumeroBanco;
  ORetorno.Header.Agencia;
  ...

// Os itens, acesse a lista Itens
  for I := 0 to ORetorno.Itens.Coun-1 do
  begin  
    ORetorno.Itens.Items[I].NumeroDocumento;
    ORetorno.Itens.Items[I].DataDeposito;
	...
  end;

  // Acessar as property's do Trailler do Arquivo
  ORetorno.Trailler.Qtde;
  ORetorno.Trailler.Valor;
end;

==================
Débito Autorizado:
==================
  Existem vários tipos de convênios que os bancos utilizam, optamos por utilizar um convênio padrão Febraban “CNAB150”, pois possuem a maioria dos campos iguais em todos os bancos.

  Existem versões diferentes no “CNAB150”, exemplo versão 4 e versão 5. Os campos são na sua maioria iguais

Exemplo:
uses Jera.DA;

procedure LerRetornoDA(ABanco: TJeraDABancos; AFileName: string);
var
  ORetorno: IJeraDA;
begin
  // Cria a classe conforme banco informado no parâmetro
  ORetorno := TJeraDA.Create(ABanco);
  // Abre o arquivo selecionado acima e processa seu conteúdo
  ORetorno.Processar(AFileName);
  
  // Acessar as property's do Header do Arquivo
  ORetorno.BlocoA.CodigoConvenio;
  ORetorno.BlocoA.NSA;
  ...

// Os itens, acesse a lista Itens
  for I := 0 to ORetorno.BlocoF.Coun-1 do
  begin  
    ORetorno.BlocoF.Items[I].DataVencimento;
    ORetorno.BlocoF.Items[I].ValorDebito;
	...
  end;

  // Acessar as property's do Trailler do Arquivo
  ORetorno.BlocoZ.TotalRegistrosArquivo;
  ORetorno.BlocoZ.ValorTotalRegistros;
end;

procedure GeraRemessa(ABanco: TJeraDABancos);
var
  IRemessa: IJeraDARemessa;
  I: Integer;
begin
  IRemessa := TJeraDARemessa.Create(ABanco);
  IRemessa.BlocoA.CodigoConvenio := '123';
  IRemessa.BlocoA.NSA := 1;
  IRemessa.BlocoA.DataGeracao := Now;
  IRemessa.BlocoA.NomedaEmpresa := 'JETER RABELO FERREIRA';
  for I := 0 to 1 do
  begin
    with IRemessa.BlocoE.New do
    begin
      IdentificacaoClienteEmpresa :='2-5'
      AgenciaDebito := '123';
      Conta := '45678';
      ContaDigito := '9';
      DataVencimento := Now;
      ValorDebito := 50;
      CodigoMoeda := cmReal;
      // Física
        TipoIdentificacao := iCPF
      // Jurídica
      //  TipoIdentificacao := iCNPJ;
      Identificacao := '123456789-00'
      IdentificacaoClienteBanco := 'ALGUNS BANCOS FORNECEM ESSE DADO';
    end;
  end;
  IRemessa.Processar;
end;


Fontes hospedados em: https://bitbucket.org/jerasoft/jera-da-di

Quaisquer dúvidas, entrar em contato.
suporte@jerasoft.com.br

Copyright by Jera Soft Co. ® - 2016
Jéter Rabelo Ferreira