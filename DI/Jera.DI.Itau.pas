{******************************************************************************}
{ Projeto: TJeraDIItau                                                         }
{                                                                              }
{ Fun��o: Fazer a leitura de arquivo magn�tico recebidos de bancos, referente  }
{         Dep�sitos Identificados - Banco Itau                                 }
{                                                                              }
{         Essas rotinas n�o fazem a gera��o do mesmo, pois isso n�o � previsto }
{         nessa modalidade de servi�o prestado pelos bancos.                   }
{                                                                              }
{  Esta biblioteca � software livre; voc� pode redistribu�-la e/ou modific�-la }
{ sob os termos da Licen�a P�blica Geral Menor do GNU conforme publicada pela  }
{ Free Software Foundation; tanto a vers�o 2.1 da Licen�a, ou (a seu crit�rio) }
{ qualquer vers�o posterior.                                                   }
{                                                                              }
{  Esta biblioteca � distribu�da na expectativa de que seja �til, por�m, SEM   }
{ NENHUMA GARANTIA; nem mesmo a garantia impl�cita de COMERCIABILIDADE OU      }
{ ADEQUA��O A UMA FINALIDADE ESPEC�FICA. Consulte a Licen�a P�blica Geral Menor}
{ do GNU para mais detalhes. (Arquivo LICEN�A.TXT ou LICENSE.TXT)              }
{                                                                              }
{  Voc� deve ter recebido uma c�pia da Licen�a P�blica Geral Menor do GNU junto}
{ com esta biblioteca; se n�o, escreva para a Free Software Foundation, Inc.,  }
{ no endere�o 59 Temple Street, Suite 330, Boston, MA 02111-1307 USA.          }
{ Voc� tamb�m pode obter uma copia da licen�a em:                              }
{ http://www.opensource.org/licenses/lgpl-license.php                          }
{******************************************************************************}

{******************************************************************************}
{ Direitos Autorais Reservados � 2016 - J�ter Rabelo Ferreira                  }
{ Contato: jeter.rabelo@jerasoft.com.br                                        }
{******************************************************************************}
unit Jera.DI.Itau;

interface

uses
  System.SysUtils, System.Classes, Jera.DI, Jera.DADI.Util;

type
  TJeraDIHItau = class(TInterfacedObject, IFinanceitoDIH)
  private
    [DadosDefinicoes(fdatString, 91, 3)]
    FNumeroBanco: string;

    [DadosDefinicoes(fdatString, 4, 4)]
    FAgencia: string;

    [DadosDefinicoes(fdatString, 8, 5)]
    FNumContaCorrente: string;

    [DadosDefinicoes(fdatString, 13, 1)]
    FNumContaCorrenteDigito: string;

    [DadosDefinicoes(fdatInteger, 85, 6)]
    FNumeroSequencialArq: Integer;

    function GetNumeroBanco: string;
    procedure SetNumeroBanco(const Value: string);
    function GetAgencia: string;
    procedure SetAgencia(const Value: string);
    function GetNumContaCorrente: string;
    procedure SetNumContaCorrente(const Value: string);
    function GetNumContaCorrenteDigito: string;
    procedure SetNumContaCorrenteDigito(const Value: string);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);
    function GetClasse: TClass;
  public
    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property NumeroBanco: string read GetNumeroBanco write SetNumeroBanco;
    property Agencia: string read GetAgencia write SetAgencia;
    property NumContaCorrente: string read GetNumContaCorrente write SetNumContaCorrente;
    property NumContaCorrenteDigito: string read GetNumContaCorrenteDigito write SetNumContaCorrenteDigito;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

  TJeraDIItauItens = class(TInterfacedObject, IJeraDII)
  private
    [DadosDefinicoes(fdatData, 16, 6, 'YYMMDD')]
    FDataDeposito: TDate;

    [DadosDefinicoes(fdatString, 22, 4)]
    FAgenciaAcolhedora: string;

    [DadosDefinicoes(fdatString, 0, 0)]
    FAgenciaAcolhedoraDigito: string;

    [DadosDefinicoes(fdatString, 35, 16)]
    FIdentificador: string;

    [DadosDefinicoes(fdatMoney, 55, 17)]
    FValorTotal: Double;

    [DadosDefinicoes(fdatInteger, 0, 0)]
    FNumeroDocumento: Integer;

    [DadosDefinicoes(fdatInteger, 85, 6)]
    FNumeroSequencialArq: Integer;

    function GetDataDeposito: TDate;
    procedure SetDataDeposito(const Value: TDate);
    function GetAgenciaAcolhedora: string;
    procedure SetAgenciaAcolhedora(const Value: string);
    function GetAgenciaAcolhedoraDigito: string;
    procedure SetAgenciaAcolhedoraDigito(const Value: string);
    function GetIdentificador: string;
    procedure SetIdentificador(const Value: string);
    function GetValorTotal: Double;
    procedure SetValorTotal(const Value: Double);
    function GetNumeroDocumento: Integer;
    procedure SetNumeroDocumento(const Value: Integer);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);
    function GetClasse: TClass;
  public
    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property DataDeposito: TDate read GetDataDeposito write SetDataDeposito;
    property AgenciaAcolhedora: string read GetAgenciaAcolhedora write SetAgenciaAcolhedora;
    property AgenciaAcolhedoraDigito: string read GetAgenciaAcolhedoraDigito write SetAgenciaAcolhedoraDigito;
    property Identificador: string read GetIdentificador write SetIdentificador;
    property ValorTotal: Double read GetValorTotal write SetValorTotal;
    property NumeroDocumento: Integer read GetNumeroDocumento write SetNumeroDocumento;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

  TJeraDIItauTrailler = class(TInterfacedObject, IFinanceitoDIT)
  private
    [DadosDefinicoes(fdatMoney, 62, 17)]
    FValor: Double;

    [DadosDefinicoes(fdatInteger, 79, 6)]
    FQtde: Integer;

    [DadosDefinicoes(fdatInteger, 85, 6)]
    FNumeroSequencialArq: Integer;

    function GetQtde: Integer;
    procedure SetQtde(const Value: Integer);
    function GetValor: Currency;
    procedure SetValor(const Value: Currency);
    function GetNumeroSequencialArq: Integer;
    procedure SetNumeroSequencialArq(const Value: Integer);
    function GetClasse: TClass;
  public
    procedure CarregarLinha(const Value: string);
    property CLasse: TClass read GetClasse;
    property Valor: Currency read GetValor write SetValor;
    property Qtde: Integer read GetQtde write SetQtde;
    property NumeroSequencialArq: Integer read GetNumeroSequencialArq write SetNumeroSequencialArq;
  end;

implementation

uses
  StrUtils;

{ TJeraDIItauItens }

procedure TJeraDIItauItens.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

function TJeraDIItauItens.GetAgenciaAcolhedora: string;
begin
  Result := FAgenciaAcolhedora;
end;

function TJeraDIItauItens.GetAgenciaAcolhedoraDigito: string;
begin
  Result := FAgenciaAcolhedoraDigito;
end;

function TJeraDIItauItens.GetClasse: TClass;
begin
  Result := Self.ClassType;
end;

function TJeraDIItauItens.GetDataDeposito: TDate;
begin
  Result := FDataDeposito;
end;

function TJeraDIItauItens.GetIdentificador: string;
begin
  Result := FIdentificador;
end;

function TJeraDIItauItens.GetNumeroDocumento: Integer;
begin
  Result := FNumeroDocumento;
end;

function TJeraDIItauItens.GetNumeroSequencialArq: Integer;
begin
  Result := FNumeroSequencialArq;
end;

function TJeraDIItauItens.GetValorTotal: Double;
begin
  Result := FValorTotal;
end;

procedure TJeraDIItauItens.SetAgenciaAcolhedora(const Value: string);
begin
  FAgenciaAcolhedora := Value;
end;

procedure TJeraDIItauItens.SetAgenciaAcolhedoraDigito(
  const Value: string);
begin
  FAgenciaAcolhedoraDigito := Value;
end;

procedure TJeraDIItauItens.SetDataDeposito(const Value: TDate);
begin
  FDataDeposito := Value;
end;

procedure TJeraDIItauItens.SetIdentificador(const Value: string);
var
  I: Integer;
  SId: string;
begin
  FIdentificador := Value;
//   Separar o d�gito
  SId := Copy(FIdentificador, 1, Length(FIdentificador)-1);
// Montar o identificador N-D
  FIdentificador := IntToStr(StrToIntDef(SId, 0)) + '-' + RightStr(Value, 1);
end;

procedure TJeraDIItauItens.SetNumeroDocumento(const Value: Integer);
begin
  FNumeroDocumento := Value;
end;

procedure TJeraDIItauItens.SetNumeroSequencialArq(
  const Value: Integer);
begin
  FNumeroSequencialArq := Value;
end;

procedure TJeraDIItauItens.SetValorTotal(const Value: Double);
begin
  FValorTotal := Value;
end;

{ TJeraDIItauTrailler }

procedure TJeraDIItauTrailler.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

function TJeraDIItauTrailler.GetClasse: TClass;
begin
  Result := Self.ClassType;
end;

function TJeraDIItauTrailler.GetNumeroSequencialArq: Integer;
begin
  Result := FNumeroSequencialArq;
end;

function TJeraDIItauTrailler.GetQtde: Integer;
begin
  Result := FQtde;
end;

function TJeraDIItauTrailler.GetValor: Currency;
begin
  Result := FValor;
end;

procedure TJeraDIItauTrailler.SetNumeroSequencialArq(
  const Value: Integer);
begin
  FNumeroSequencialArq := Value;
end;

procedure TJeraDIItauTrailler.SetQtde(const Value: Integer);
begin
  FQtde := Value;
end;

procedure TJeraDIItauTrailler.SetValor(const Value: Currency);
begin
  FValor := Value;
end;

{ TJeraDIHItau }

procedure TJeraDIHItau.CarregarLinha(const Value: string);
begin
  TJeraDADIUtils.CarregarLinhaToObj(Value, Self);
end;

function TJeraDIHItau.GetAgencia: string;
begin
  Result := FAgencia;
end;

function TJeraDIHItau.GetClasse: TClass;
begin
  Result := Self.ClassType;
end;

function TJeraDIHItau.GetNumContaCorrente: string;
begin
  Result := FNumContaCorrente;
end;

function TJeraDIHItau.GetNumContaCorrenteDigito: string;
begin
  Result := FNumContaCorrenteDigito
end;

function TJeraDIHItau.GetNumeroBanco: string;
begin
  Result := FNumeroBanco;
end;

function TJeraDIHItau.GetNumeroSequencialArq: Integer;
begin
  Result := FNumeroSequencialArq;
end;

procedure TJeraDIHItau.SetAgencia(const Value: string);
begin
  FAgencia := Value;
end;

procedure TJeraDIHItau.SetNumContaCorrente(const Value: string);
begin
  FNumContaCorrente := Value;
end;

procedure TJeraDIHItau.SetNumContaCorrenteDigito(const Value: string);
begin
  FNumContaCorrenteDigito := Value;
end;

procedure TJeraDIHItau.SetNumeroBanco(const Value: string);
begin
  FNumeroBanco := Value;
end;

procedure TJeraDIHItau.SetNumeroSequencialArq(const Value: Integer);
begin
  FNumeroSequencialArq := Value;
end;

end.



